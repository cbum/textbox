//******************************************************************************
enum class DrawStyle : unsigned
{
  FillAndStroke,
  Stroke,
  StrokeAndFill,
  Fill
};


//******************************************************************************
//
class ThBLTextBox
{
public:
  enum class Align
  {
    Left,
    Right,
    Center,
    Justify
  };


private:
  typedef struct _line_
  {
    unsigned uiLinesCount{};   // in first line of paragraph specify lines count in paragraph
    unsigned uiBeginOffset{};  // offset from begin of text (start position of line)
    unsigned uiLen{};          // length of line
    unsigned uiBreaksCount{};  // count of breaks in line
    double dBreaksExtra{};     // size of free space (not used by line) in out width
  } LINE;

  typedef struct _params_
  {
    Array<unsigned> arrForcedBreaksIndex;
    Array<unsigned> arrBreaksPos;
    Array<double> arrSpansWidth;
    Array<LINE> arrLines;
    FixedString strText;
    Align eAlign{ Align::Left };
    double dIndent{};
    double dFontHeight{};
    double dStringHeight{};
    double dSpaceSize{};                   // width of glyph ' ' (space)
    double dNLSize{};                      // width of glyph '\n' (new line)
    double dLineSpacing{ 1.0 };
  } PARAMS;


protected:
  PARAMS* _pParams{ new PARAMS() };


public:
  ~ThBLTextBox()
  {
    delete _pParams;
    _pParams = nullptr;
  }

  ThBLTextBox()
  {
  }

  ThBLTextBox(PCWSTR szText)
  {
    _pParams->strText = szText;

    _parseText();
  }


public:
  void draw(BLContext& ct, BLRect& rc, BLFont& fnt, const DrawStyle ds = DrawStyle::Fill)
  {
    if (_pParams->strText.isEmpty())
    {
      return;
    }

    ct.save();
    ct.clipToRect(rc);
    _draw(ct, rc, fnt, ds);
    ct.restore();
  }

  void cdraw(BLContext& ct, BLRect& rc, BLFont& fnt, const DrawStyle ds = DrawStyle::Fill)
  {
    if (_pParams->strText.isEmpty())
    {
      return;
    }

    ct.save();
    ct.clipToRect(rc);
    _calcSpans(fnt);
    _fillLines(rc.w);
    _draw(ct, rc, fnt, ds);
    ct.restore();
  }

  inline void calc(BLFont& fnt)
  {
    _calcSpans(fnt);
  }

  inline void layout(const double width)
  {
    _fillLines(width);
  }


public:
  inline void setText(PCWSTR szText)
  {
    _pParams->strText = szText;

    _parseText();
  }

  inline void setIndent(const double dIndent)
  {
    _pParams->dIndent = dIndent;
  }

  inline void setAlign(const Align eAlign)
  {
    _pParams->eAlign = eAlign;
  }

  inline void setLineSpacing(const double dSpacing)
  {
    _pParams->dLineSpacing = dSpacing;
  }

  inline const FixedString& getText(void)
  {
    return _pParams->strText;
  }

  inline const double& getIndent(void)
  {
    return _pParams->dIndent;
  }

  inline const Align& getAlign(void)
  {
    return _pParams->eAlign;
  }

  inline const double& getLineSpacing(void)
  {
    return _pParams->dLineSpacing;
  }

  inline const unsigned getLinesCount(void)
  {
    return (unsigned)_pParams->arrLines.getItemsCount();
  }

  inline const double getHeight(void)
  {
    return (double)_pParams->arrLines.getItemsCount() * (_pParams->dStringHeight * _pParams->dLineSpacing);
  }


protected:
  void _draw(BLContext& ct, BLRect& rc, BLFont& fnt, const DrawStyle ds)
  {
    if (_pParams->arrLines.getItemsCount() == 0)
    {
      return;
    }

    const double dBottom     = rc.y + rc.h;
    const double dLineHeight = _pParams->dStringHeight * _pParams->dLineSpacing;
    unsigned uiLinesLeft     = 0;
    BLPoint pt(0.0, rc.y + _pParams->dFontHeight);

    for (LINE& Line : _pParams->arrLines)
    {
      if (Line.uiLen != 0)
      {
        int iBreakExtraSpaceSize = 0;

        switch (_pParams->eAlign)
        {
          case Align::Right:
            pt.x = rc.x + Line.dBreaksExtra;
            break;

          case Align::Center:
            pt.x = rc.x + Line.dBreaksExtra * 0.5;
            break;

          case Align::Justify:
            iBreakExtraSpaceSize = (int)(float)(Line.dBreaksExtra / Line.uiBreaksCount / fnt.matrix().m00 + 0.5);

          default:
            pt.x = rc.x;
            break;
        }

        if (Line.uiLinesCount != 0)  // begin of paragraph
        {
          switch (_pParams->eAlign)
          {
            case Align::Center:
              pt.x += _pParams->dIndent * 0.5;
              break;

            default:
              pt.x += _pParams->dIndent;
              break;
          }

          uiLinesLeft = Line.uiLinesCount;
        }

        BLGlyphBuffer gb;
        gb.setWCharText(_pParams->strText.str() + Line.uiBeginOffset, Line.uiLen);
        fnt.shape(gb);
        fnt.applyKerning(gb);

        if (iBreakExtraSpaceSize != 0 && uiLinesLeft > 1)
        {
          unsigned pos         = 0;
          BLGlyphPlacement* gp = gb.placementData();

          do
          {
            pos = _pParams->strText.findFirstOf(L" \t", pos + Line.uiBeginOffset, 3);
            pos -= Line.uiBeginOffset;
            gp[pos].advance.x += iBreakExtraSpaceSize;
          } while (++pos < Line.uiLen);
        }

        const BLGlyphRun& gr = gb.glyphRun();

        switch (ds)
        {
          case DrawStyle::FillAndStroke:
            ct.fillGlyphRun(pt, fnt, gr);
          case DrawStyle::Stroke:
            ct.strokeGlyphRun(pt, fnt, gr);
            break;

          case DrawStyle::StrokeAndFill:
            ct.strokeGlyphRun(pt, fnt, gr);
          case DrawStyle::Fill:
          default:
            ct.fillGlyphRun(pt, fnt, gr);
            break;
        }

        --uiLinesLeft;
      }

      {  // if y >= bottom --> return
        const double cmp = pt.y - dBottom;
        if ((int)(abs(cmp) / cmp) >= 0) return;
      }

      pt.y += dLineHeight;
    }
  }

  void _parseText(void)
  {
    _pParams->arrBreaksPos.clear();
    _pParams->arrForcedBreaksIndex.clear();
    _pParams->arrLines.clear();

    if (_pParams->strText.isEmpty())
    {
      return;
    }

    unsigned uiPos = 0;

    while ((uiPos = _pParams->strText.findFirstOf(L" \n\t", uiPos, 4)) != FixedString::npos)
    {
      _pParams->arrBreaksPos.append(uiPos);

      if (_pParams->strText[uiPos] == L'\n')
      {
        _pParams->arrForcedBreaksIndex.append(_pParams->arrBreaksPos.getItemsCount() - 1);
      }

      ++uiPos;
    }

    _pParams->arrForcedBreaksIndex.append(_pParams->arrBreaksPos.getItemsCount() - 1);  // add index of text length

    _pParams->arrBreaksPos.shrinkToFit();
    _pParams->arrForcedBreaksIndex.shrinkToFit();
  }

  void _calcSpans(BLFont& fnt)
  {
    _pParams->arrSpansWidth.clear();

    const double& m00 = fnt.matrix().m00;

    for (unsigned& uiSpanLen : _pParams->arrBreaksPos)
    {
      BLGlyphBuffer gb;
      gb.setWCharText(_pParams->strText.str(), uiSpanLen);
      fnt.shape(gb);
      fnt.applyKerning(gb);

      BLGlyphPlacement* pgp = gb.placementData();
      const size_t gcnt     = gb.size();
      int iWidth            = 0;
      for (size_t i = 0; i < gcnt; ++i)
      {
        iWidth += pgp[i].advance.x;
      }
      _pParams->arrSpansWidth.append(iWidth * m00);
    }

    _pParams->arrSpansWidth.shrinkToFit();

    BLGlyphBuffer gb;

    //**************************************************************************
    gb.setWCharText(L" ", 1);
    fnt.shape(gb);
    _pParams->dSpaceSize = (double)gb.placementData()[0].advance.x * m00;

    //**************************************************************************
    gb.setWCharText(L"\n", 1);
    fnt.shape(gb);
    //fnt.applyKerning(gb); // ???
    _pParams->dNLSize = (double)gb.placementData()[0].advance.x * m00;

    //**************************************************************************
    const BLFontMetrics& fm = fnt.metrics();
    _pParams->dFontHeight   = (double)fm.ascent;
    _pParams->dStringHeight = (double)fm.ascent + (double)fm.descent + (double)fm.lineGap;
  }

  void _fillLines(const double dWidth)
  {
    _pParams->arrLines.clear();

    unsigned uiPrevForcedIndex           = 0;
    unsigned uiFirstLineIndex            = 0;
    unsigned uiTotalLinesCount           = 0;

    for (unsigned& uiForcedIndex : _pParams->arrForcedBreaksIndex)
    {
      const unsigned uiStartPos = (uiTotalLinesCount == 0 ? 0 : (_pParams->arrBreaksPos[uiPrevForcedIndex] + 1));
      const unsigned uiSpanLen  = _pParams->arrBreaksPos[uiForcedIndex] - uiStartPos;
      unsigned uiCharCount;
      unsigned uiFirstLineChartCount;
      {
        const double dAveCharWidth = (_pParams->arrSpansWidth[uiForcedIndex] - (uiTotalLinesCount == 0 ? 0 : (_pParams->arrSpansWidth[uiPrevForcedIndex]) + _pParams->dNLSize)) / (double)uiSpanLen;
        uiCharCount                = (unsigned)(float)(dWidth / dAveCharWidth) - 1;
        uiFirstLineChartCount      = uiCharCount - (unsigned)(float)(_pParams->dIndent / dAveCharWidth) - 1;
      }

      if (_pParams->arrBreaksPos[uiForcedIndex] == uiStartPos)
      {
        // add empty paragraph
        _pParams->arrLines.append(LINE{ 1,
                                        0,
                                        0,
                                        0,
                                        dWidth });

        ++uiTotalLinesCount;
      }
      else
      {
        FixedString strText(_pParams->strText.getSubString(uiStartPos, uiSpanLen));

        unsigned uiLen = strText.findLastOf(L" \t", uiFirstLineChartCount, 3);
        if (uiLen == FixedString::npos)
        {
          uiLen = strText.findFirstOf(L" \t", uiFirstLineChartCount, 3);
        }

        unsigned uiBreakIndex = (unsigned)_pParams->arrBreaksPos.find(uiStartPos + uiLen);
        uiFirstLineIndex = (unsigned)_pParams->arrLines.append(LINE{ 0,
                                                                     uiStartPos,
                                                                     uiLen,
                                                                     uiBreakIndex - uiPrevForcedIndex - (uiTotalLinesCount == 0 ? 0 : 1),
                                                                     dWidth - _pParams->dIndent - _pParams->arrSpansWidth[uiBreakIndex] + (uiTotalLinesCount == 0 ? 0 : (_pParams->arrSpansWidth[uiPrevForcedIndex] + _pParams->dNLSize)) });

        while (uiBreakIndex < uiForcedIndex)
        {
          const unsigned uiPrevLen        = uiLen + 1;
          const unsigned uiPrevBreakIndex = uiBreakIndex;
          
          uiLen = strText.findLastOf(L" \t", uiPrevLen + uiCharCount, 3);
          if (uiPrevLen > uiLen)
          {
            uiLen = strText.findFirstOf(L" \t", uiPrevLen + uiCharCount, 3);
          }

          uiBreakIndex = (unsigned)_pParams->arrBreaksPos.find(uiStartPos + uiLen);
          _pParams->arrLines.append(LINE{ 0,
                                          uiStartPos + uiPrevLen,
                                          uiLen - uiPrevLen,
                                          uiBreakIndex - uiPrevBreakIndex - 1,
                                          dWidth - _pParams->arrSpansWidth[uiBreakIndex] + _pParams->arrSpansWidth[uiPrevBreakIndex] + _pParams->dSpaceSize });
        }

        const size_t& nLinesCnt                           = _pParams->arrLines.getItemsCount();
        _pParams->arrLines[uiFirstLineIndex].uiLinesCount = (unsigned)nLinesCnt - uiTotalLinesCount;  // lines count in paragraph
        uiTotalLinesCount                                 = (unsigned)nLinesCnt;                      // total lines created
      }

      uiPrevForcedIndex = uiForcedIndex;
    }

    _pParams->arrLines.shrinkToFit();
  }

};  // class ThBLTextBox
