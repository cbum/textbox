//******************************************************************************
enum TextOutFormat : unsigned
{
  TOF_LEFT      = 0x00000000,
  TOF_CENTER    = 0x00000001,
  TOF_RIGHT     = 0x00000002,
  TOF_TOP       = 0x00000000,
  TOF_MIDDLE    = 0x00000010,
  TOF_BOTTOM    = 0x00000020,
  TOF_WORDBREAK = 0x00000100
};


//******************************************************************************
enum class DrawStyle : unsigned
{
  FillAndStroke,
  Stroke,
  StrokeAndFill,
  Fill
};


//******************************************************************************
//
class ThBLLabelBox
{
private:
  typedef struct _line_
  {
    unsigned uiBeginOffset{};  // offset from begin of text (start position of line)
    unsigned uiLen{};          // length of line
    double dBreaksExtra{};     // size of free space (not used by line) in out width
  } LINE;


  typedef struct _params_
  {
    Array<unsigned> arrBreaksPos;
    Array<unsigned> arrForcedBreaksIndex;
    Array<double> arrSpansWidth;
    Array<LINE> arrLines;
    FixedString strText;
    unsigned uiFormat{ TOF_LEFT | TOF_TOP };
    double dFontHeight{};
    double dStringHeight{};
    double dSpaceSize{};                      // width of glyph ' ' (space)
    double dNLSize{};                         // width of glyph '\n' (new line)
  } PARAMS;


protected:
  PARAMS* _pParams{ new PARAMS() };


public:
  ~ThBLLabelBox()
  {
    delete _pParams;
    _pParams = nullptr;
  }

  ThBLLabelBox()
  {
  }

  ThBLLabelBox(PCWSTR szText)
  {
    _pParams->strText = szText;

    _parseText();
  }


public:
  void draw(BLContext& ct, BLRect& rc, BLFont& fnt, const DrawStyle ds = DrawStyle::Fill)
  {
    if (_pParams->strText.isEmpty())
    {
      return;
    }

    _draw(ct, rc, fnt, ds);
  }

  void cdraw(BLContext& ct, BLRect& rc, BLFont& fnt, const DrawStyle ds = DrawStyle::Fill)
  {
    if (_pParams->strText.isEmpty())
    {
      return;
    }

    _calcSpans(fnt);
    _fillLines(rc.w);
    _draw(ct, rc, fnt, ds);
  }

  inline void calc(BLFont& fnt)
  {
    _calcSpans(fnt);
  }

  inline void layout(const double width)
  {
    _fillLines(width);
  }


public:
  inline void setText(PCWSTR szText)
  {
    _pParams->strText = szText;

    _parseText();
  }

  inline void setFormat(const unsigned fmt)
  {
    _pParams->uiFormat = fmt;
  }

  inline const FixedString& getText(void)
  {
    return _pParams->strText;
  }

  inline const unsigned& getFormat(void)
  {
    return _pParams->uiFormat;
  }


protected:
  void _draw(BLContext& ct, BLRect& rc, BLFont& fnt, const DrawStyle ds)
  {
    BLPoint pt(rc.x, rc.y + _pParams->dFontHeight);

    switch (_pParams->uiFormat & 0x000000F0)
    {
      case TOF_MIDDLE:
        pt.y += (rc.h - _pParams->arrLines.getItemsCount() * _pParams->dStringHeight) * 0.5;
        break;

      case TOF_BOTTOM:
        pt.y += rc.h - _pParams->arrLines.getItemsCount() * _pParams->dStringHeight;
        break;

      default:
        break;
    }

    for (const LINE& line : _pParams->arrLines)
    {
      if (line.uiLen != 0)
      {
        switch (_pParams->uiFormat & 0x0000000F)
        {
          case TOF_CENTER:
            pt.x = rc.x + line.dBreaksExtra * 0.5;
            break;

          case TOF_RIGHT:
            pt.x = rc.x + line.dBreaksExtra;
            break;

          default:
            break;
        }

        BLGlyphBuffer gb;
        gb.setWCharText(_pParams->strText.str() + line.uiBeginOffset, line.uiLen);
        fnt.shape(gb);
        fnt.applyKerning(gb);
        const BLGlyphRun& gr = gb.glyphRun();

        switch (ds)
        {
          case DrawStyle::FillAndStroke:
            ct.fillGlyphRun(pt, fnt, gr);
          case DrawStyle::Stroke:
            ct.strokeGlyphRun(pt, fnt, gr);
            break;

          case DrawStyle::StrokeAndFill:
            ct.strokeGlyphRun(pt, fnt, gr);
          case DrawStyle::Fill:
          default:
            ct.fillGlyphRun(pt, fnt, gr);
            break;
        }
      }

      pt.y += _pParams->dStringHeight;
    }
  }

  void _parseText(void)
  {
    _pParams->arrBreaksPos.clear();
    _pParams->arrForcedBreaksIndex.clear();
    _pParams->arrLines.clear();

    if (_pParams->strText.isEmpty())
    {
      return;
    }

    unsigned uiPos = 0;

    while ((uiPos = _pParams->strText.findFirstOf(L" \n\t", uiPos, 4)) != FixedString::npos)
    {
      _pParams->arrBreaksPos.append(uiPos);

      if (_pParams->strText[uiPos] == L'\n')
      {
        _pParams->arrForcedBreaksIndex.append(_pParams->arrBreaksPos.getItemsCount() - 1);
      }

      ++uiPos;
    }

    _pParams->arrForcedBreaksIndex.append(_pParams->arrBreaksPos.getItemsCount() - 1);

    _pParams->arrBreaksPos.shrinkToFit();
    _pParams->arrForcedBreaksIndex.shrinkToFit();
  }

  void _calcSpans(BLFont& fnt)
  {
    _pParams->arrSpansWidth.clear();

    const double& m00 = fnt.matrix().m00;

    for (unsigned& uiSpanLen : _pParams->arrBreaksPos)
    {
      BLGlyphBuffer gb;
      gb.setWCharText(_pParams->strText.str(), uiSpanLen);
      fnt.shape(gb);
      fnt.applyKerning(gb);

      BLGlyphPlacement* pgp = gb.placementData();
      const size_t gcnt     = gb.size();
      int iWidth            = 0;
      for (size_t i = 0; i < gcnt; ++i)
      {
        iWidth += pgp[i].advance.x;
      }
      _pParams->arrSpansWidth.append(iWidth * m00);
    }

    _pParams->arrSpansWidth.shrinkToFit();

    BLGlyphBuffer gb;

    //**************************************************************************
    gb.setWCharText(L" ", 1);
    fnt.shape(gb);
    _pParams->dSpaceSize = (double)gb.placementData()[0].advance.x * m00;

    //**************************************************************************
    gb.setWCharText(L"\n", 1);
    fnt.shape(gb);
    //fnt.applyKerning(gb); // ???
    _pParams->dNLSize = (double)gb.placementData()[0].advance.x * m00;

    //**************************************************************************
    const BLFontMetrics& fm = fnt.metrics();
    _pParams->dFontHeight   = (double)fm.ascent;
    _pParams->dStringHeight = (double)fm.ascent + (double)fm.descent + (double)fm.lineGap;
  }

  void _fillLines(const double dWidth)
  {
    if ((_pParams->uiFormat & TOF_WORDBREAK) != 0)
    {
      unsigned uiPrevForcedIndex = 0;

      for (unsigned& uiForcedIndex : _pParams->arrForcedBreaksIndex)
      {
        const unsigned uiStartPos = (uiPrevForcedIndex == 0 ? 0 : (_pParams->arrBreaksPos[uiPrevForcedIndex] + 1));
        const unsigned uiSpanLen  = _pParams->arrBreaksPos[uiForcedIndex] - uiStartPos;
        unsigned uiCharCount;
        {
          const double dAveCharWidth = (_pParams->arrSpansWidth[uiForcedIndex] - (uiPrevForcedIndex == 0 ? 0 : (_pParams->arrSpansWidth[uiPrevForcedIndex]) + _pParams->dNLSize)) / (double)uiSpanLen;
          uiCharCount                = (unsigned)(float)(dWidth / dAveCharWidth) - 1;
        }

        if (_pParams->arrBreaksPos[uiForcedIndex] == uiStartPos)
        {
          _pParams->arrLines.append(LINE{ 0,
                                          0,
                                          dWidth });
        }
        else
        {
          FixedString strText(_pParams->strText.getSubString(uiStartPos, uiSpanLen));

          unsigned uiLen = strText.findLastOf(L" \t", uiCharCount, 3);
          if (uiLen == FixedString::npos)
          {
            uiLen = strText.findFirstOf(L" \t", uiCharCount, 3);
          }

          unsigned uiBreakIndex = (unsigned)_pParams->arrBreaksPos.find(uiStartPos + uiLen);
          _pParams->arrLines.append(LINE{ uiStartPos,
                                          uiLen,
                                          dWidth - _pParams->arrSpansWidth[uiBreakIndex] + (uiPrevForcedIndex == 0 ? 0 : (_pParams->arrSpansWidth[uiPrevForcedIndex] + _pParams->dNLSize)) });

          while (uiForcedIndex > uiBreakIndex)
          {
            const unsigned uiPrevLen        = uiLen + 1;
            const unsigned uiPrevBreakIndex = uiBreakIndex;
            
            uiLen = strText.findLastOf(L" \t", uiPrevLen + uiCharCount, 3);
            if (uiPrevLen > uiLen)
            {
              uiLen = strText.findFirstOf(L" \t", uiPrevLen + uiCharCount, 3);
            }

            uiBreakIndex = (unsigned)_pParams->arrBreaksPos.find(uiStartPos + uiLen);
            _pParams->arrLines.append(LINE{ uiStartPos + uiPrevLen,
                                            uiLen - uiPrevLen,
                                            dWidth - _pParams->arrSpansWidth[uiBreakIndex] + _pParams->arrSpansWidth[uiPrevBreakIndex] + _pParams->dSpaceSize });
          }
        }

        uiPrevForcedIndex = uiForcedIndex;
      }
    }
    else
    {
      unsigned uiPrevForcedIndex = 0;

      for (unsigned& uiForcedIndex : _pParams->arrForcedBreaksIndex)
      {
        const unsigned uiStartPos = (uiPrevForcedIndex == 0 ? 0 : (_pParams->arrBreaksPos[uiPrevForcedIndex] + 1));

        if (_pParams->arrBreaksPos[uiForcedIndex] == uiStartPos)
        {
          _pParams->arrLines.append(LINE{ 0,
                                          0,
                                          dWidth });
        }
        else
        {
          _pParams->arrLines.append(LINE{ uiStartPos,
                                          _pParams->arrBreaksPos[uiForcedIndex] - uiStartPos,
                                          dWidth - _pParams->arrSpansWidth[uiForcedIndex] + (uiPrevForcedIndex == 0 ? 0 : (_pParams->arrSpansWidth[uiPrevForcedIndex] + _pParams->dNLSize)) });
        }

        uiPrevForcedIndex = uiForcedIndex;
      }
    }

    _pParams->arrLines.shrinkToFit();
  }
};  // class ThBLLabel
