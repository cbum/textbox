//******************************************************************************
enum TextOutFormat : unsigned
{
  TOF_LEFT      = 0x00000000,
  TOF_CENTER    = 0x00000001,
  TOF_RIGHT     = 0x00000002,
  TOF_TOP       = 0x00000000,
  TOF_MIDDLE    = 0x00000010,
  TOF_BOTTOM    = 0x00000020,
  TOF_WORDBREAK = 0x00000100
};


//******************************************************************************
enum class DrawStyle : unsigned
{
  FillAndStroke,
  Stroke,
  StrokeAndFill,
  Fill
};


//******************************************************************************
typedef struct _line_
{
  unsigned uiBeginOffset{};  // offset of begin "word span"
  unsigned uiLen{};          // len of "word span"
  double dBreaksExtra{};     // free space in out rect width
} LINE;


void textOut(PCWSTR szText, BLRect& rc, BLFont& fnt, const uint32_t fmt = TOF_LEFT | TOF_TOP, const DrawStyle ds = DrawStyle::Fill)
{
  if (szText == nullptr || lstrlenW(szText) == 0)
  {
    return;
  }

  Array<unsigned> arrBreaksPos;
  Array<double> arrSpansWidth;
  Array<LINE> arrLines;

  if ((fmt & TO_WORDBREAK) != 0)
  {
    Array<unsigned> arrForcedBreaksIndex;

    //**************************************************************************
    {
      FixedString strText(szText);
      unsigned uiPos    = 0;
      const double& m00 = fnt.matrix().m00;

      while ((uiPos = strText.findFirstOf(L" \n\t", uiPos, 4)) != FixedString::npos)
      {
        arrBreaksPos.append(uiPos); // append break position

        if (szText[uiPos] == L'\n') // forced break found
        {
          arrForcedBreaksIndex.append(arrBreaksPos.getItemsCount() - 1); // save index of forced break
        }

        BLGlyphBuffer gb;
        gb.setWCharText(szText, uiPos);
        fnt.shape(gb);
        fnt.applyKerning(gb);

        BLGlyphPlacement* pgp = gb.placementData();
        const size_t gcnt     = gb.size();
        int iWidth            = 0;
        for (size_t i = 0; i < cnt; ++i)
        {
          iWidth += pgp[i].advance.x;
        }
        arrSpansWidth.append(iWidth * m00);

        ++uiPos;
      }

      arrForcedBreaksIndex.append(arrBreaksPos.getItemsCount() - 1); // append index of text length
    }

    //**************************************************************************
    {
      double dSpaceSize;
      double dNLSize;

      {
        const double& m00 = fnt.matrix().m00;
        BLGlyphBuffer gb;

        gb.setWCharText(L" ", 1);
        fnt.shape(gb);
        dSpaceSize = (double)gb.placementData()[0].advance.x * m00;

        gb.setWCharText(L"\n", 1);
        fnt.shape(gb);
        //fnt.applyKerning(gb); // ???
        dNLSize = (double)gb.placementData()[0].advance.x * m00;
      }

      unsigned uiPrevForcedIndex = 0;

      for (unsigned& uiForcedIndex : arrForcedBreaksIndex)
      {
        const unsigned uiStartPos = (isFirst ? 0 : (arrBreaksPos[uiPrevForcedIndex] + 1));
        const unsigned uiSpanLen  = arrBreaksPos[uiForcedIndex] - uiStartPos;
        unsigned uiCharCount;
        {
          const double dAveCharWidth = (arrSpansWidth[uiForcedIndex] - (uiPrevForcedIndex == 0 ? 0 : (arrSpansWidth[uiPrevForcedIndex]) + dNLSize)) / (double)uiSpanLen;
          uiCharCount                = (unsigned)(float)(rc.w / dAveCharWidth) - 1;
        }

        if (arrBreaksPos[uiForcedIndex] == uiStartPos)
        {
          arrLines.append(LINE{ 0,
                                0,
                                rc.w });
        }
        else
        {
          FixedString strText;
          strText.set(szText + uiStartPos, uiSpanLen);

          unsigned uiLen = strText.findLastOf(L" \t", uiCharCount, 3);
          if (uiLen == FixedString::npos)
          {
            uiLen = strText.findFirstOf(L" \t", uiCharCount, 3);
          }

          unsigned uiBreakIndex = (unsigned)arrBreaksPos.find(uiStartPos + uiLen);
          arrLines.append(LINE{ uiStartPos,
                                uiLen,
                                rc.w - arrSpansWidth[uiBreakIndex] + (isFirst ? 0 : (arrSpansWidth[uiPrevForcedIndex] + dNLSize)) });

          while (uiForcedIndex > uiBreakIndex)
          {
            const unsigned uiPrevLen        = uiLen + 1;
            const unsigned uiPrevBreakIndex = uiBreakIndex;

            uiLen = strText.findLastOf(L" \t", uiPrevLen + uiCharCount, 3);
            if (uiPrevLen > uiLen)
            {
              uiLen = strText.findFirstOf(L" \t", uiPrevLen + uiCharCount, 3);
            }

            uiBreakIndex = (unsigned)arrBreaksPos.find(uiStartPos + uiLen);
            arrLines.append(LINE{ uiStartPos + uiPrevLen,
                                  uiLen - uiPrevLen,
                                  rc.w - arrSpansWidth[uiBreakIndex] + arrSpansWidth[uiPrevBreakIndex] + dSpaceSize });
          }
        }

        uiPrevForcedIndex = uiForcedIndex;
      }
    }
  }
  else  // without TO_WORDBREAK
  {
    //**************************************************************************
    {
      FixedString strText(szText);
      unsigned uiPos    = 0;        // 
      const double& m00 = fnt.matrix().m00;
      bool end          = false;

      do
      {
        uiPos = strText.find(L'\n', uiPos); // find "new line" position

        if (uiPos == FixedString::npos) // if not found
        {
          uiPos = strText.getLength(); // set length of text
          end   = true;
        }

        arrBreaksPos.append(uiPos);

        BLGlyphBuffer gb;
        gb.setWCharText(szText, uiPos);
        fnt.shape(gb);
        fnt.applyKerning(gb);

        BLGlyphPlacement* pgp = gb.placementData();
        const size_t gcnt     = gb.size();  // get count of glyphs
        int iWidth            = 0;
        for (size_t i = 0; i < gcnt; ++i)
        {
          iWidth += pgp[i].advance.x;
        }
        arrSpansWidth.append(iWidth * m00);

        ++uiPos;
      } while (!end);
    }

    //**************************************************************************
    {
      double dNLSize; // width in pixels of symbol\glyph "\n"

      {
        const double& m00 = fnt.matrix().m00;
        BLGlyphBuffer gb;

        gb.setWCharText(L"\n", 1);
        fnt.shape(gb);
        //fnt.applyKerning(gb); // ???
        dNLSize = (double)gb.placementData()[0].advance.x * m00;
      }

      const unsigned uiBreaksCnt = (UINT)arrBreaksPos.getItemsCount();
      for (UINT i = 0; i < uiBreaksCnt; ++i)
      {
        const unsigned uiStartPos  = (i == 0 ? 0 : (arrBreaksPos[i - 1] + 1));
        const unsigned& uiBreakPos = arrBreaksPos[i];

        if (uiBreakPos == uiStartPos)
        {
          arrLines.append(LINE{ 0,
                                0,
                                rc.w });
        }
        else
        {
          arrLines.append(LINE{ uiStartPos,
                                uiBreakPos - uiStartPos,
                                rc.w - arrSpansWidth[i] + (i == 0 ? 0 : (arrSpansWidth[i - 1] + dNLSize)) });
        }
      }
    }
  }  // without TO_WORDBREAK

  //**************************************************************************
  // draw lines
  {
    const BLFontMetrics& fm    = fnt.metrics();
    const double dStringHeight = (double)fm.ascent + (double)fm.descent + (double)fm.lineGap;
    BLPoint pt(rc.x, rc.y + fm.ascent);

    switch (fmt & 0x000000F0)
    {
      case TOF_MIDDLE:
        pt.y += (rc.h - arrLines.getItemsCount() * dStringHeight) * 0.5;
        break;

      case TOF_BOTTOM:
        pt.y += rc.h - arrLines.getItemsCount() * dStringHeight;
        break;

      default:
        break;
    }

    for (const LINE& line : arrLines)
    {
      if (line.uiLen != 0)
      {
        switch (fmt & 0x0000000F)
        {
          case TOF_CENTER:
            pt.x = rc.x + line.dBreaksExtra * 0.5;
            break;

          case TOF_RIGHT:
            pt.x = rc.x + line.dBreaksExtra;
            break;

          default:
            break;
        }

        BLGlyphBuffer gb;
        gb.setWCharText(szText + line.uiBeginOffset, line.uiLen);
        fnt.shape(gb);
        fnt.applyKerning(gb);
        const BLGlyphRun& gr = gb.glyphRun();

        switch (ds)
        {
          case DrawStyle::FillAndStroke:
            m_Context.fillGlyphRun(pt, fnt, gr);
          case DrawStyle::Stroke:
            m_Context.strokeGlyphRun(pt, fnt, gr);
            break;

          case DrawStyle::StrokeAndFill:
            m_Context.strokeGlyphRun(pt, fnt, gr);
          case DrawStyle::Fill:
          default:
            m_Context.fillGlyphRun(pt, fnt, gr);
            break;
        }
      }

      pt.y += dStringHeight;
    }
  }
}
